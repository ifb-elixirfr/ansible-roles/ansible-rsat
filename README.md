Ansible RSAT
===============

A role to install RSAT from https://github.com/rsa-tools

Unsing the method described here: https://rsa-tools.github.io/installing-RSAT/unix-install-rsat/installing_RSAT_procedure.html


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)
